"""
Class for handling web pages and their contents
"""

import os
import requests

import const
from locators import Locators
from helper_methods import HelperMethods as methods


class WebContent:

    def __init__(self, driver, page_url):
        self.driver = driver
        self.url = page_url

    def __str__(self):
        return 'Page URL: %s' % self.url

    def load_page(self):
        """
        Loads page content
        :return:
        """
        self.driver.get(self.url)

    def get_paragraph_content(self):
        """
        Locates paragraphs on the page and
        :return:
        """
        all_text = str()

        paragraphs = self.driver.find_elements(*Locators.PARAGRAPH_DIV)
        for paragraph in paragraphs:
            text = paragraph.text
            all_text += ' %s' % text

        return all_text

    def is_image_present_on_page(self, path_of_image_to_compare_with):
        """
        Takes in a path of an image to compare to, downloads found image(s) and compares to requested one,
        if image found, function exits returning True, otherwise continues and returns False after checking all images
        :param path_of_image_to_compare_with:
        :return: bool
        """
        compare_with = path_of_image_to_compare_with
        extension = path_of_image_to_compare_with.split('.')[-1]

        # get all images in main paragraph section
        example_images = self.driver.find_elements(*Locators.PARAGRAPH_IMAGES)

        for index, image in enumerate(example_images):
            # download image
            current_link = image.get_attribute("src")
            response = requests.get(current_link)
            if response.status_code == 200:
                current_img_name = '.'.join(['current_img', extension])
                current_img_path = os.path.join(const.images_folder, current_img_name)
                with open(current_img_path, 'wb') as file:
                    file.write(response.content)
                # compare downloaded image to punisher, return True and exit if found
                if methods.are_two_images_same(current_img_path, compare_with):
                    print('Image detected! Exiting...')
                    os.remove(current_img_path)
                    result = True
                    return result
                else:
                    os.remove(current_img_path)
                    continue
        # return false if none is matching
        return False

# shape_automation_coding_challenge

Test framework for "The Internet" page.
Tests will run headlessly (no browser window will open)

Contains of 2 tests:

1. test passes if there's a word longer than 10 characters in paragraphs
2. test passes if Punisher image does not appear on the page


Prerequisites:

1. Python 3;
2. Selenium;
3. Chromedriver;
4. Python modules:
- requests library ("pip install requests");
- PIL library (pip install Pillow).

To run test from command line:

- clone this repository;
- cd to shape_automation_coding_challenge;
- run "python run_page_test.py";

Results will be displayed in terminal.
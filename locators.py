"""
A class for main page locators. All main page locators should come here
"""

from selenium.webdriver.common.by import By


class Locators():

    # The Internet Page
    PARAGRAPH_DIV = \
        (By.XPATH, '//div[@class="large-10 columns large-centered"]/div/div[@class="large-10 columns"]')
    PARAGRAPH_IMAGES = \
        (By.XPATH, '//div[@class="large-10 columns large-centered"]/div/div[@class="large-2 columns"]/img')
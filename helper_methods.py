"""
Helper methods
"""

from PIL import Image


class HelperMethods():

    @staticmethod
    def is_word_found(input_text, word_length):
        """
        Takes in string. Searches for word with specified length or longer
        returns the first word found that satisfies length criteia.
        :param input_text:
        :param word_length:
        :return: word str
        """
        if input_text:
            words = input_text.replace('.', '').strip().split(' ')
            for word in words:
                length = len(word)
                if length > (int(word_length) - 1):
                    print('%s char word found: %s' % (length, word))
                    return True
                else:
                    continue
            return False

    @staticmethod
    def get_longest_word(input_text):
        """
        Takes in string. Searches for longest word in provided string text
        returns longest word found
        :param input_text:
        :return: current_longest
        """
        if input_text:
            current_longest = None
            words = input_text.replace('.', '').strip().split(' ')
            for word in words:
                current_length = len(word)
                if current_longest is None:
                    current_longest = word
                elif current_length > len(current_longest):
                    current_longest = word

            return current_longest

    @staticmethod
    def are_two_images_same(path_to_image1, path_to_image2):
        """
        Compare two images, return True if same, False if not
        :param path_to_image1:
        :param path_to_image2:
        :return: bool
        """
        img1 = Image.open(path_to_image1)
        img2 = Image.open(path_to_image2)
        if img1 == img2:
            return True
        else:
            return False

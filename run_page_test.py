"""
Execution of tests for "The internet" page
"""

import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

import const
from web_page import WebContent
from helper_methods import HelperMethods as methods

chrome_options = Options()
chrome_options.add_argument("--headless")

# test constants (shared with all tests)
the_internet_page_url = const.the_internet_page_url


class TheInternetPage(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(options=chrome_options)

    def load_page(self):
        the_internet_page = WebContent(self.driver, the_internet_page_url)
        the_internet_page.load_page()

        return the_internet_page

    def test_if_10_characters_or_longer_word_found(self):

        internet_page = self.load_page()
        page_text = internet_page.get_paragraph_content()
        longest_word = methods.get_longest_word(page_text)
        word_len = len(longest_word)

        print('test_if_10_characters_or_longer_word_found: %s - %s chars.' % (longest_word, word_len))
        self.assertTrue(word_len >= 10)

    def test_ensure_punisher_image_is_not_on_the_page(self):

        internet_page = self.load_page()
        bool = internet_page.is_image_present_on_page(const.punisher_image)
        if bool:
            print('test_ensure_punisher_image_is_not_on_the_page: Punisher found!')
        self.assertFalse(bool)

    def tearDown(self):
        self.driver.close()


if __name__ == '__main__':
    unittest.main()

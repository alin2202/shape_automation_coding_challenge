""" Constans for project files"""

import os

#### The Internet Page ####
the_internet_page_url = 'https://the-internet.herokuapp.com/dynamic_content'
the_internet_page_url_statis = 'https://the-internet.herokuapp.com/dynamic_content?with_content=static'
images_folder = 'images'
punisher_image = os.path.join(images_folder, 'Punisher.jpg')